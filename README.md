# Backoffice Seperator for umbraco 7#

### How do I get set up? ###

Just download and add the "CodeMonkey.Seperator" folder to your "App_Plugins" inside your umbraco project. Now you can create these lovely seperators for your backoffice. Giving even futher understanding for your editors/users.


![cm.seperator.PNG](https://bitbucket.org/repo/7XkRrL/images/3323347787-cm.seperator.PNG)