﻿angular.module("umbraco").controller("CodeMonkey.SeperatorController", function ($scope) {

	function formatDisplayValue() {

		if($scope.model.label !== null && $scope.model.label !== "") {
			$scope.displayvalue = $scope.model.label;
		}
		else if($scope.model.config.seperatorText !== null && $scope.model.config.seperatorText !== "") {
        	$scope.displayvalue = $scope.model.config.seperatorText;
	    }
	    else {
	        $scope.displayvalue = "";
	    }
       	//console.log($scope.model.label);
    }

    //format the display value on init:
    formatDisplayValue();
});